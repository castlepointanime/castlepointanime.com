.PHONE: build clean test deploy

# Build stages

build: _site

test: build node_modules/jshint/bin/jshint
	node_modules/jshint/bin/jshint resources/js
	node_modules/jslint/bin/jslint.js --nomen --plusplus --browser resources/js/*

deploy: build
	bundle install
	bundle exec s3_website push

clean:
	-$(RM) -rf resources/vendor
	-$(RM) -rf vendor/
	-$(RM) -rf node_modules/


# File targets

_site: vendor/bin/brancher resources/vendor _config.yml src data resources templates
	vendor/bin/brancher build

resources/vendor: node_modules/bower/bin/bower
	bower install

node_modules/bower/bin/bower: package.json
	npm install

node_modules/jshint/bin/jshint: package.json
	npm install --dev

node_modules/jslint/bin/jslint.js: package.json
	npm install --dev

vendor/bin/brancher: composer.phar composer.json
	./composer.phar install -n -o

composer.phar:
	curl -sS https://getcomposer.org/installer | php
