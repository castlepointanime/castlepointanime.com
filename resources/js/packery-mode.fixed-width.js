/*global $, document, Isotope, getSize */
// external js: isotope.pkgd.js, packery-mode.pkgd.js
$(document).ready(function () {
    "use strict";

    // overwrite Packery methods
    var packeryMode = Isotope.LayoutMode.modes.packery,
        resetLayout = packeryMode.prototype._resetLayout;

    packeryMode.prototype._resetLayout = function () {
        resetLayout.call(this);

        // reset packer
        var parentSize = getSize(this.element.parentNode),
            colW = this.columnWidth + this.gutter,
            maxWidth = $(this.element).css('max-width');

        this.fitWidth = Math.floor((parentSize.innerWidth + this.gutter) / colW) * colW;
        if (maxWidth !== "none") {
            this.fitWidth = Math.min(this.fitWidth, parseInt(maxWidth, 10));
        }
        this.packer.width = this.fitWidth;
        this.packer.height = Number.POSITIVE_INFINITY;
        this.packer.reset();
    };

    packeryMode.prototype._getContainerSize = function () {
        return {
            width: this.fitWidth - this.gutter,
            height: this.maxY - this.gutter
        };
    };

    // always resize
    packeryMode.prototype.needsResizeLayout = function () {
        return true;
    };
});
