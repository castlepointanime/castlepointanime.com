/*global $, document, window */
$(document).ready(function () {
    "use strict";

    // Set up isotope.
    var container = $(".isotope-container").isotope({
        layoutMode: "packery",
        itemSelector: ".isotope-item",
        sortBy: "original-order",
        isInitLayout: false,
        packery: {
            columnWidth: 270,
            gutter: 20,
            isFitWidth: true
        }
    });

    // Isotope timeout rerun.
    setTimeout(function () {
        container.isotope('layout');
    }, 100);

    // Isotope filters.
    $("#isotope-all").click(function () {
        $(".isotope-container").isotope({filter: '*'});
    });
    $("#isotope-industryGuests").click(function () {
        $(".isotope-container").isotope({filter: '.industryGuests'});
    });
    $("#isotope-featuredPanels").click(function () {
        $(".isotope-container").isotope({filter: '.featuredPanels'});
    });
    $("#isotope-cosplay").click(function () {
        $(".isotope-container").isotope({filter: '.cosplay'});
    });
    $("#isotope-musicalActs").click(function () {
        $(".isotope-container").isotope({filter: '.musicalActs'});
    });

    // Auto-ellipsis script.
    $(".clamp").ellipsis();

    // Scrollspy when needed.
    $("body").scrollspy({
        "target": ".sidebar-nav",
        "offset": 40
    });

    // Same-page anchor fix.
    $(window).on('hashchange', function () {
        var d = $(window).scrollTop();
        d = Math.max(d - 60, 0);
        $(window).scrollTop(d);
    });
});
